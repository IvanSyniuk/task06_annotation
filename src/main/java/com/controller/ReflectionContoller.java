package com.controller;

import com.model.Car;
import com.model.ClassInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class ReflectionContoller {
    private static Logger logger = LogManager.getLogger(ReflectionContoller.class);
    private Car car = new Car();
    private Class clas = car.getClass();


    public void showInfo() {
        ClassInfo classInfo = new ClassInfo(car);
        classInfo.showInfo();
    }

    public void invokeCountAndShowAgeCar() {
        try {
            Method method = clas.getDeclaredMethod("countAndShowAgeCar");
            method.setAccessible(true);
            method.invoke(car);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void invokeCountAgeCar() {
        try {
            Method method = clas.getDeclaredMethod("countAgeCar");
            method.setAccessible(true);
            logger.info(method.invoke(car));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void setValue(String scanner) {
        try {
            Field field = clas.getDeclaredField("brand");
            field.setAccessible(true);
            field.set(car, scanner);
            logger.info(field.get(car));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void invokeCountPriceInUah() {
        try {
            Method method = clas.getDeclaredMethod("countPriceInUah");
            method.setAccessible(true);
            logger.info(method.invoke(car));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}