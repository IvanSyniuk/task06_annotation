package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;

public class Car {

    private static Logger logger = LogManager.getLogger(Car.class);

    @CarInfo(brand = "Mercedes")
    private String brand;

    @CarInfo(model = "E-class")
    private String model;

    @CarInfo(yearCar = 2000)
    private int yearCar;

    int price = 10000;

    private void countAndShowAgeCar() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        logger.info(year - this.yearCar);
    }

    private int countAgeCar() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year - yearCar;
    }

    private int countPriceInUah() {
        int exchange = 26;
        return price * exchange;
    }
}