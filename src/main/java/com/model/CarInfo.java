package com.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CarInfo {

    String brand() default "carBrand";

    String model() default "carModel";

    int yearCar() default 2019;
}
