package com.model;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ClassInfo {

    private static Logger log = LogManager.getLogger(ClassInfo.class);
    private Class clas;

    public ClassInfo(Object object) {
        clas = object.getClass();
    }

    public void showInfo() {
        log.info("Class name : " + clas.getSimpleName());
        for (Field field : clas.getDeclaredFields()) {
            log.info("Field : " + field.getType() + " " + field.getName());
        }
        for (Method method : clas.getDeclaredMethods()) {
            log.info("Method : " + method.getReturnType() + " " + method.getName());
        }
        for (Constructor constructor : clas.getDeclaredConstructors()) {
            log.info("Constructor : " + constructor.getName() + " " +
                    Arrays.toString(constructor.getParameterTypes()));
        }
    }
}
