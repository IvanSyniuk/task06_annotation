package com.view;

import com.controller.ReflectionContoller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MainView {
    static Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MainView.class);

    ReflectionContoller reflectionContoller;

    public void showMenu() {
        reflectionContoller = new ReflectionContoller();
        boolean flag = true;
        do {
            menu();
            switch (scanner.nextInt()) {
                case 1:
                    reflectionContoller.invokeCountAgeCar();
                    break;
                case 2:
                    reflectionContoller.invokeCountAndShowAgeCar();
                    break;
                case 3:
                    reflectionContoller.invokeCountPriceInUah();
                    break;
                case 4:
                    reflectionContoller.showInfo();
                    break;
                case 5:
                    reflectionContoller.setValue(scanner.next());
                    break;
                case 6:
                    flag = false;
            }
        } while (flag);
    }

    public void menu() {
        logger.info("\n1:invokeCountAgeCar\n2:invokeCountAndShowAgeCar\n:3invokeCountPriceInUah\n4:showInfo\n5:setValue\n6:EXIT");
    }
}
